<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="">
  <meta name="author" content="Kavin Prasad">
  <meta name="generator" content="Hugo 0.101.0">
  <title>Photogram</title>
  <link href="./assets/dist/css/bootstrap.min.css" rel="stylesheet">
  <link rel="stylesheet" href="./assets/dist/css/style.css">

  <? if (file_exists($_SERVER['DOCUMENT_ROOT'] . '/php-class-projects/photogram/css/' . basename($_SERVER['PHP_SELF'], ".php") . ".css")) { ?>
    <link rel="stylesheet" href="./css/<?= basename($_SERVER['PHP_SELF'], ".php"); ?>.css">
  <? } ?>

</head>