<?
$signup = false;
if (isset($_POST['username']) and isset($_POST['password']) and !empty(isset($_POST['password'])) and isset($_POST['email_address']) and isset($_POST['phone'])) {
  $username = $_POST['username'];
  $password = $_POST['password'];
  $email = $_POST['email_address'];
  $phone = $_POST['phone'];
  $error = User::signup($username, $password, $email, $phone);
  $signup = true;
}



if ($signup) {
  if (!$error) {

?>
    <main class="form-signup w-100 m-auto">
      <img class="mb-4" src="./assets/brand/logo.png" alt="" width="80" height="80">
      <h1 class="h3 mb-3 fw-normal">Login Success you can login <a href="./login.php">here</a>.</h1>
      <p class="mt-5 mb-3 text-muted">&copy; 2017–2022</p>
    </main>
  <!-- <div class="container-fluid bg-primary p-1 text-light">
    <h5>Login Success you can login <a class="text-light" href="./login.php">here</a>.</h5>
  </div> -->


  <?
  } else {
    // print_r($error);

  ?>

    <main class="form-signup w-100 m-auto">
      <img class="mb-4" src="./assets/brand/logo.png" alt="" width="80" height="80">
      <h1 class="h3 mb-3 fw-normal">Login Failed Retry Again <br>.<a href="./signup.php">here</a>.</h1>
      <h1 class="h3 mb-3 fw-normal"></h1>
      <p class="mt-5 mb-3 text-muted">&copy; 2017–2022</p>
    </main>

  <? } ?>

<?
} else {
?>


  <main class="form-signup w-100 m-auto">
    <form method="post" action="signup.php">
      <img class="mb-4" src="./assets/brand/logo.png" alt="" width="80" height="80">
      <h1 class="h3 mb-3 fw-normal">Register Here</h1>

      <div class="form-floating">
        <input name="username" type="text" class="form-control" id="floatingInput" placeholder="kavinprasad">
        <label for="floatingInput">Username</label>
      </div>
      <div class="form-floating">
        <input name="phone" type="text" class="form-control" id="floatingInput" placeholder="9876543210">
        <label for="floatingInput">Phone</label>
      </div>
      <div class="form-floating">
        <input name="email_address" type="email" class="form-control" id="floatingInput" placeholder="name@example.com">
        <label for="floatingInput">Email address</label>
      </div>
      <div class="form-floating">
        <input name="password" type="password" class="form-control" id="floatingPassword" placeholder="Password">
        <label for="floatingPassword">Password</label>
      </div>

      <div class="checkbox mb-3">
        <label>
          <input type="checkbox" value="remember-me"> Remember me
        </label>
      </div>
      <button class="w-100 btn btn-lg btn-primary" type="submit">Register</button>
      <p class="mt-5 mb-3 text-muted">&copy; 2017–2022</p>
    </form>
  </main>
<?
}

?>