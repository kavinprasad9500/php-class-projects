<?php
include 'libs/load.php';

$user = "kavin";
$pass = isset($_GET['pass']) ? $_GET['pass'] : '';
$result = null;

if (isset($_GET['logout'])) {
    Session::destroy();
    die("Session destroyed <br><br><br><br> <a href='loginTest.php'><Button>Login Again</Button></a>");
}

/*
1. Check if sessio_token in PHP Session is valid.
2. If yes, construct UserSession and see if its successfull.
3. Check if the session is valid one.
4. If valid, print "Session Validated".
5. Else print "Invalid Session" and the user in login.
*/
if (Session::get('is_loggedin')) {
    $username = Session::get('session_username');
    $userobj = new User($username);
    print("Welcome Back ".$userobj->getFirstname());
    // print("<br>".$userobj->getBio());
    $userobj->setBio("Making new things...");
    // print("<br>".$userobj->getBio());
} else {
    printf("No session found <br>");
    $result = User::login($user, $pass);
    
    if ($result) {
        $userobj = new User($user);
        echo "Login Success ", $userobj->getUsername();
        Session::set('is_loggedin', true);
        Session::set('session_username', $result);
    } else {
        echo "Login failed, $user <br>";
    }
}

?>
<br><br><a href="loginTest.php?logout"><Button>Logout</Button></a>

