<?php

    $time = microtime(true);
    $options = [
        'cost' => 4,
    ];
    echo password_hash("f9iiig2zzo", PASSWORD_BCRYPT, $options);
    echo (" Time Took : " .microtime(true) - $time) . "sec". "\n";

    if (password_verify("f9iiig2zzo",'$2y$09$5PGDEiq0qlmo4HMSHVgb6uott1ZMIi6B9Agon6a5caWFliKtP7Er6')){
        print("Correct Password");
    }else{
        print("Wrong Password");
    }
