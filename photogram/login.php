<?php
include 'libs/load.php';

?>


  <!doctype html>
  <html lang="en">


  <?php load_template('_head'); ?>


  <body class="text-center">

    <?php load_template('_header') ?>


    <?php load_template('_login') ?>

    <?php load_template('_footer') ?>

    <script src="./assets/dist/js/bootstrap.bundle.min.js"></script>

  </body>

  </html>