<?php
    class Mic{
        
        private $brand;
        public $color;
        public $usb_port;
        public $model;
        private $light;
        public $price;

        public static $test;

        public function __call($name, $arguments){
            print("$name <br>");
            print_r($arguments);
            print("<br>");
        }

        public static function testFunction(){
            print("Hello World");
            print(Mic::$test);
        }

        public function __construct($brand){
            print("Constucting Object...."."\n");
            $this->brand = ucwords($brand);
        }

        public function getBrand(){
            return $this->brand;
        }

        public function setLight($light){
            $this->light = $light;
        }

        private function getModel(){
            return $this->model;
        }

        public function setModel($model){
            $this->model =ucwords($model);
        }

        public function getModelProxy(){
            return $this->getModel();
        }

        public function __destruct(){
            print("Destruct object");
        }
    }
?>